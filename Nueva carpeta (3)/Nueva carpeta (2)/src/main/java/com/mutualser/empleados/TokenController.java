package com.mutualser.empleados;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

@RestController
public class TokenController {

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Value("${jwt.secret}")
    private String jwtSecret;

    @PostMapping("/api/token")
    public String generateToken(@RequestBody UserCredentials userCredentials) {
        // Validar el usuario y contraseña
        final UserDetails userDetails = userDetailsService.loadUserByUsername(userCredentials.getUsername());

        if (!userCredentials.getPassword().equals(userDetails.getPassword())) {
            throw new UnauthorizedException("Credenciales inválidas");
        }

        // Generar un token JWT
        Key signingKey = new SecretKeySpec(DatatypeConverter.parseBase64Binary(jwtSecret), SignatureAlgorithm.HS256.getJcaName());

        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 3600000)) // 1 hora de expiración
                .signWith(signingKey)
                .compact();
    }
}
