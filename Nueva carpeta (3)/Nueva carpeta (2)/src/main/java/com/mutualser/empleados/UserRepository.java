package com.mutualser.empleados;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserCredentials, Long> {
    UserCredentials findByUsername(String username);
}
