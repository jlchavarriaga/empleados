package com.mutualser.empleados;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // Buscar el usuario en la base de datos por nombre de usuario
        UserCredentials user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Usuario no encontrado con el nombre de usuario: " + username);
        }

        // Crear un UserDetails a partir de los datos del usuario
        return org.springframework.security.core.userdetails.User
                .withUsername(username)
                .password(user.getPassword())
                // Puedes agregar roles y permisos aquí si es necesario
                .roles("USER")
                .build();
    }
}
